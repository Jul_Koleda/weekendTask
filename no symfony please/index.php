<!DOCTYPE html>
<html lang="en">
<head>

    <link href="includes/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <link href="includes/form_styles.css" rel="stylesheet">

    <meta charset="UTF-8">
    <title>Create form with pure PHP (No Symfony)</title>
    <link rel="shortcut icon" href="2.ico" type="image/x-icon">

</head>

<body>


<h2>Create form with pure PHP (No Symfony) </h2>
<div class='bd-example'>
    <form method="post" action="">

        <div class="form-group">

            <label for="name">Name</label>
            <input type="text" class="form-control" id="name"
                   maxlength="20"
                   placeholder="what is your name?" name="input_name" required/>
        </div>


        <div class="form-group">
            <label for="surname">Surname</label>
            <input name="input_surname" class="form-control" id="surname" auto-validate pattern="[a-Z]*"
                   maxlength="20"
                   placeholder="what is your surname?" required/>
        </div>


        <div class="form-check">
            <!--            <label>Gender </label>-->
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="input_optionsRadios" value="Male"
                       checked=""/>
                Male
            </label>
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="input_optionsRadios" value="Female"/>
                Female
            </label>
        </div>


        <div class="form-group">
            <label for="height">Height</label>
            <input type="number" name="input_height" class="form-control" id="height" auto-validate pattern="[0-9]*"
                   placeholder="from 100 to 250 sm" min="100" max="250" required/>
        </div>

        <div class="col-10">
            <label for="birth">Birth Date</label>
            <input class="form-control" type="date" value="2011-08-19" id="birth" name="input_birght" required/>
        </div>


        <div class="form-check">
            <label for="married" class="form-check-label">
                Married?
                <input type="checkbox" class="form-check-input" id="married" name="input_married" value="married">
            </label>
        </div>


        <div class="form-group">
            <label>Country</label>
            <label>
                <select name="input_country">
                    <option>Russia</option>
                    <option>Usa</option>
                    <option>Canada</option>
                    <option>Romania</option>
                    <option>Belarus</option>
                </select>
            </label>
        </div>
        <button type="submit" class="btn btn-success" href="index.php">Send</button>
    </form>

</div>

</body>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<table class="table">
    <tr>


        <th>First name</th>
        <td> <?php
            if (empty($_POST['input_name'])) {
                echo 'empty';
            } else {
                echo $_POST['input_name'];
            } ?> <br></td>

    </tr>


    <tr>
        <th>Second name</th>
        <td> <?php
            if (empty($_POST['input_surname'])) {
                echo 'empty';
            } else {
                echo $_POST['input_surname'];
            } ?> <br></td>

    </tr>
    <tr>
        <th>Gender</th>
        <td> <?php echo $_POST['input_optionsRadios']; ?> <br></td>

    </tr>
    <tr>
        <th>Height</th>

        <td> <?php
            if (empty($_POST['input_surname'])) {
                echo 'empty';
            } else {
                echo $_POST['input_height'];
            } ?> <br></td>
    </tr>
    <tr>
        <th>Birth date</th>
        <td> <?php echo $_POST['input_birght']; ?> <br></td>
    </tr>
    <tr>
        <th>Married</th>
        <td> <?php
            echo $_POST['input_married'];
            $mar = 'input_married';
            if ($mar) {
                echo 'not married';
            } else {
                echo 'married';
            }
            ?> <br></td>
    </tr>
    <tr>
        <th>Country</th>
        <td> <?php echo $_POST['input_country']; ?> <br></td>
    </tr>

</table>
</html>
